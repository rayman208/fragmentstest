package com.example.bottomnavigationviewexample;

import android.os.Bundle;

import android.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class fragment1 extends Fragment {

    TextView textViewF1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment1, container, false);

        textViewF1 = view.findViewById(R.id.textViewF1);

        Button buttonF1 = view.findViewById(R.id.buttonF1);
        buttonF1.setOnClickListener(buttonF1ClickListener);

        return view;
    }

    View.OnClickListener buttonF1ClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            textViewF1.setText("hello world");
        }
    };
}