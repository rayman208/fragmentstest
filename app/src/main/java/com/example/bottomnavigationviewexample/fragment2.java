package com.example.bottomnavigationviewexample;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class fragment2 extends Fragment {

    TextView textViewF2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment2, container, false);

        textViewF2 = view.findViewById(R.id.textViewF2);
        Button buttonF2 = view.findViewById(R.id.buttonF2);
        buttonF2.setOnClickListener(onButtonF2ClickListener);

        return view;
    }

    View.OnClickListener onButtonF2ClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            textViewF2.setText("hello world");
        }
    };

}