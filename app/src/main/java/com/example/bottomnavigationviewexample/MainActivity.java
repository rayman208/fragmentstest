package com.example.bottomnavigationviewexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    fragment1 f1;
    fragment2 f2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        f1 = new fragment1();
        f2 = new fragment2();

        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentsContainer, f1);
        fragmentTransaction.commit();

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item)
                    {
                        FragmentTransaction fragmentTransaction;
                        fragmentTransaction = getFragmentManager().beginTransaction();

                        switch (item.getItemId())
                        {
                            case R.id.action_map:
                                fragmentTransaction.replace(R.id.fragmentsContainer, f1);
                                break;
                            case R.id.action_dial:
                                fragmentTransaction.replace(R.id.fragmentsContainer, f2);
                                break;
                            case R.id.action_mail:

                                break;
                        }

                        fragmentTransaction.commit();

                        return true;
                    }
                });
    }
}